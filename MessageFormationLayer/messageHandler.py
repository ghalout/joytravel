
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# from django.db import models
from django.conf import settings
from DataAccessLayer.dbHandler import getAsianBotData

import random

# import random
# Create your models here.
from SimpleMessageTemplates import MessageTemplates as SimpleMessageTemplates
SimpleMessageTemplates = SimpleMessageTemplates[settings.BOT_PERSONA]


# Different type of components/inputs are defined here
def createMessage(uiComponentTrayFromDecisionLayer):

    updatedComponents = []

    componentProcessMap = {
        "SimpleMessage": processSimpleMessage,
        "Bubble": processBubble,
        "Form": processForm,
        "UserAccForm" : processUserAccForm,
        "TextBox" : processTextBox,
       
    }

    for component in uiComponentTrayFromDecisionLayer["uiComponents"]:
        #
        # based on component type select the specific processing function from component process map and
        # update the component
        #
        componentType = component["componentType"]
        updatedComponent = componentProcessMap[componentType](component)

        # append the updated components

        updatedComponents.append(updatedComponent)

    uiComponentTrayFromMessageLayer = {}
    uiComponentTrayFromMessageLayer["uiComponents"] = updatedComponents
    uiComponentTrayFromMessageLayer["botResponse"] = uiComponentTrayFromDecisionLayer["botResponse"]

    return uiComponentTrayFromMessageLayer


# Simple messages are processed here
def processSimpleMessage(simpleMessageComponent):

    if simpleMessageComponent["tag"]=="createMessageGreeting.basic":
        simpleMessageComponent["message"]= random.choice(SimpleMessageTemplates["createMessageGreeting"]["basic"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.namePresent":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["namePresent"]).format(simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.askForUserForm":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["askForUserForm"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.CatPresent":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["CatPresent"]).format(
            simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.catSelected":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["catSelected"]).format(
            simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.selectRegions":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["selectRegions"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.durationSubCatPresent":
        simpleMessageComponent["message"] = random.choice(
            SimpleMessageTemplates["createMessageGreeting"]["durationSubCatPresent"]).format(
            simpleMessageComponent["data"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.about":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["about"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.awsome":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["awsome"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.aboutIndia":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["aboutIndia"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.bestPlace":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["bestPlace"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.aboutIndia2":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["aboutIndia2"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.aboutIndia3":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["aboutIndia3"])
    elif simpleMessageComponent["tag"] == "createMessageGreeting.bestCountry":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageGreeting"]["bestCountry"])
    elif simpleMessageComponent["tag"] == "createMessageContactUs.basic":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageContactUs"]["basic"])
    elif simpleMessageComponent["tag"] == "createMessageUserAccForm.basic":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageUserAccForm"]["basic"])
    elif simpleMessageComponent["tag"] == "createMessageAskForOthers.basic":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageAskForOthers"]["basic"])

    elif simpleMessageComponent["tag"] == "createMessageForMainMenu.basic":
        simpleMessageComponent["message"] = random.choice(SimpleMessageTemplates["createMessageForMainMenu"]["basic"])
    return simpleMessageComponent


# UserForm is processed
def processForm(UserFormComponent):
    if UserFormComponent["tag"] == "UserDetails":
        UserFormComponent["nextCallTag"] = "Store_User_Details"
        UserFormComponent["message"] = ""
    return UserFormComponent


def processUserAccForm(UserAccForm):
    if UserAccForm["tag"] == "UserAccForm":
        UserAccForm["nextCallTag"] = "Store_User_BookYourAccommodation"
        UserAccForm["message"] = ""
        print "++++++++++++UserAccForm+++++++++++++++"
    return UserAccForm


def processBubble(Bubble):
    print Bubble
    if Bubble["tag"] == "createBotData.data":
        print Bubble["data"],'test test test'
        Bubble["data"] = getAsianBotData(Bubble["websiteId"], Bubble["parentId"])
        Bubble["nextCallTag"] = "Store_Bubble_data"

    elif Bubble["tag"] == "mainMenu.data":
        Bubble["data"] = [{u'optionName': u'Main Menu', u'optionId': 300, 'node_type_id': 16}]
        print Bubble["data"], '+++++++++++'
        Bubble["nextCallTag"] = "Store_Bubble_data"

    elif Bubble["tag"] == "firstCall.data":
        Bubble["data"] = getAsianBotData(Bubble["websiteId"], Bubble["parentId"])
        # print Bubble["data"],'+++++++++++'
        Bubble["nextCallTag"] = "Store_Bubble_data"

    return Bubble


def processTextBox(TextBox):
    if TextBox["tag"] == "TextBox":
        TextBox["nextCallTag"] = "Handle_Text_Box"    # check this
        TextBox["message"] = ""
    return TextBox
   
  