


MessageTemplates={

    "simple":{
        "createMessageGreeting":{
            "basic":[
                "Hey I am Joe ! "
            ],

            "namePresent": [
                "Thanks for your details. ",

            ],
            "askForUserForm": [
                "Kindly provide me your details so that I can serve you better.",
                # "Kindly provide me your basic details so that I can share the details of the best packages as per your own preferences. "

            ],
            "CatPresent": [
                "What kind of tour are you looking at today?"
            ],
            "catSelected":[

                # "Great! You selected <b>{0}</b>."
                "Thanks for your selection. "
            ],
            "selectRegions": [
                "In which month you are planning to travel?"

            ],
            "durationSubCatPresent": [
                "How long you want the tour to be?"
            ],
             "awsome":[
                 "Awesome!"
             ],
             "about":[
                 "I am a <b>travel expert</b> from <b>Joy Travels</b>.",

             ],
            "aboutIndia": [
                "India's culture is among the world's oldest; civilization in India began about 4,500 years ago.",

            ],
            "aboutIndia2": [
                " Facts about international travel are:",

            ],
            "aboutIndia3": [
                "1) The number of Indians travelling abroad has gone up 2.5 times in a decade.<br> 2)The UNWTO predicts that India will account for 50 million outbound tourists by 2020",

            ],
            "bestPlace":["Every state has it's own importance. Based on our knowledge I'll recommend you the best place for travel."],
            "bestCountry":["I'll recommend you the best destination according to weather."],
},
        "createMessageContactUs":{
            "basic":[
                "For further queries you can call us at +91-8506017770, +91-8506017771, +91-8506017772 or can also mail us at info@joy-travels.com"
            ],
        },
        "createMessageUserAccForm": {
            "basic": [
                    "Select your preferred travelling dates along with the number of adults and children."
            ],
        },
        "createMessageAskForOthers":{
            "basic": [
                    "Sure, please write and submit your requirements below. "
            ],
        },
        "createMessageForMainMenu":{
            "basic": [
                    # "What are you looking for now ? "
                    "What kind of tour are you looking for?"
            ],
        },
    }
}


