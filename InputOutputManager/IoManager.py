from __future__ import unicode_literals
from PaxcelBotFramework.InputOutputManager.models import SimpleInputProcessor,OutputProcessorType1
from BotManager.models  import getTemplateBotResponse
from DecisionLogicLayer.decisionLogicLayer import processBotResponse
import json
from MessageFormationLayer.messageHandler import createMessage
import os
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
from django.conf import settings
import json
import logging
logger = logging.getLogger(__name__)



@csrf_exempt
def processInputRequest(request):

    """

    This is the first function called after a request is made

    :param request: request object
    :return: an http response


    """

    ##
    ## Initialise the Input Processor Object
    ##
    print request
    if request.method == 'POST':
        requestProcessor = SimpleInputProcessor()
        requestProcessor.extractRequestData(request)
        InputRequest = requestProcessor.createBotRequest()
        responseType = requestProcessor.responseType
        responseObject = getTemplateBotResponse(InputRequest)
        decisionLogicResponse = processBotResponse(responseObject)
        userMessage = responseObject["outputMessages"][-1]["message"]

        # logger.info(
        #     "User Query: {0}".format((userMessage)),
        #     extra={'ConversationId': responseObject["botRequest"]["info"]['ConvSessionId']}
        # )
        messageLayerResponse = createMessage(decisionLogicResponse)
        outputDict = asianAdvOutputProcessorType1(outputContent=messageLayerResponse,
                                                  responseType=responseType).processRequest()

        infoField = responseObject["botRequest"]["info"]

        if "convId" and "seqNo" and "chatLogsConvId" in infoField.keys():
           if "SimpleMessage" in outputDict.keys():
               botReply = (outputDict['SimpleMessage'])
               for reply in range(len(botReply)):
                   text1 = botReply[reply].replace("<br>", "")
                   text2 = text1.replace("<b>", "")
                   text3 = text2.replace("</b>", "")

                   botReplyId = dbChatlogHandler.messagesChatlog(text3, settings.CONN_STRING)
                   seqNo = responseObject["botRequest"]['info']['seqNo']
                   seqNo = int(seqNo) + 1
                   responseObject["botRequest"]['info']['seqNo'] = seqNo
                   convId = responseObject["botRequest"]['info']['chatLogsConvId']
                   dbChatlogHandler.messagesSequenceChatlogs(convId, seqNo, botReplyId,
                                                             True, settings.CONN_STRING)
        return HttpResponse(json.dumps(outputDict, sort_keys=False, indent=4, separators=(',', ': ')),
                            content_type='application/json', charset='UTF-8')
    else:
        chatlogsConvId = request.GET.get('chatlogsConvId')
        print chatlogsConvId

        outputDict = dbChatlogHandler.chatLogsofConversation(chatlogsConvId, settings.CONN_STRING)
        return HttpResponse(json.dumps(outputDict, sort_keys=False, indent=4, separators=(',', ': ')),
                            content_type='application/json', charset='UTF-8')


class asianAdvOutputProcessorType1(OutputProcessorType1):

    def __init__(self,outputContent=None,botDisplayName="",responseType="json", uiComponentTypeFunctionMap=None):

        uiComponentTypeFunctionMap={

            'Form':asianAdvOutputProcessorType1.processForm,
            'CategoryList': asianAdvOutputProcessorType1.processCategoryList,
            'Bubble' : asianAdvOutputProcessorType1.Bubble,
            #"SimpleMessage":CRMBotOutputProcessorType1.processSimpleMessage
            'UserAccForm': asianAdvOutputProcessorType1.processUserAccForm,
            'TextBox': asianAdvOutputProcessorType1.processTextBox,

        }

        super(asianAdvOutputProcessorType1, self).__init__(outputContent=outputContent,botDisplayName=botDisplayName,
                                                           responseType=responseType,uiComponentTypeFunctionMap=uiComponentTypeFunctionMap)


    def processForm(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                            {"emailId":"email","required":"True"},
                            {"phoneNo":"numeric","required":"True"},
                            {"address": "text", "required": "False"}

                            ],
                    "componentType":"Form",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"UserDetails",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "Form" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["Form"].append(uiComponent)
            else:
                self.outputDict["Form"] = [uiComponent]


#  ----------------
    def processCategoryList(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data": "",  # from data access layer
            "tag": "FAQlist",  # this is for message formation layer
            "componentType": "FAQlist",
            "message": "",  # introduce message
            "name": str(uuid.uuid4())  # a random name everytime

                }
        """
        #uiComponent.pop("data",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)


        if self.responseType == "json":
            if "CategoryList" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["CategoryList"].append(uiComponent)
            else:
                self.outputDict["CategoryList"] = [uiComponent]

#  ----------------
    def Bubble(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"

        uiComponent.pop("message",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("name", None)
        uiComponent.pop("componentType", None)


        if self.responseType == "json":
            if "Bubble" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["Bubble"].append(uiComponent)
            else:
                self.outputDict["Bubble"] = [uiComponent]

    def processUserAccForm(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                            {"emailId":"email","required":"True"},
                            {"phoneNo":"numeric","required":"True"},
                            {"address": "text", "required": "False"}

                            ],
                    "componentType":"Form",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"UserDetails",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "UserAccForm" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["UserAccForm"].append(uiComponent)
            else:
                self.outputDict["UserAccForm"] = [uiComponent]

    def processTextBox(self,uiComponent):
        """

        This function loads the bot brain into the outputDict, returns modified output dict with the bot brain added .

        :param outputDict: A dictionary object , similar to the output structure
        :param responseObject: MessageLayerModel object which contains the response to the front end
        :return: modified outputDict (with the bot brain added)

        """
        #
        # botObject = [el for el in responseObject.content if isinstance(el, BotBrain)][0]
        #

        #if "BotBrain" in self.outputDict.keys():
            # if self.responseType=="json"
        """

        {
        responseElement=
                    "data":[
                            {"name":"text","required":"True"},
                           ],
                    "componentType":"TextBox",
                    "message":"",
                    "name":str(uuid.uuid4()),  # a random name everytime
                    "tag":"TextBox",
                    "nextCallTag": ""

                }
        """
        uiComponent.pop("name",None)
        uiComponent.pop("tag",None)
        uiComponent.pop("componentType", None)

        if self.responseType == "json":
            if "TextBox" in self.outputDict.keys():
                # if self.responseType=="json"
                self.outputDict["TextBox"].append(uiComponent)
            else:
                self.outputDict["TextBox"] = [uiComponent]




