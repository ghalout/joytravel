import requests
import json
from dumper import dump


headers = {"content-type": "application/x-www-form-urlencoded"}


def getAsianBotData(website_Id,data_Id):
    url = 'http://34.230.154.142:7351/getAsianAdvDataByWebisteIdAndNodeId'
    # url = 'http://34.230.154.142:8482/getAsianAdvDataByWebisteIdAndNodeId'
    params = {'parent_website_Id': website_Id,'node_type_id':"-1","parent_data_Id":data_Id}
    response = requests.post(url, data=params, headers=headers)
    data = response.json()
    print data
    return data


def getWebsiteId(domain):
    url = 'http://34.230.154.142:7351/getWebsiteId'
    # url = 'http://34.230.154.142:8482/getWebsiteId'
    params = {'url': domain}
    response = requests.post(url, data=params, headers=headers)
    data = response.json()
    print data[0]["websiteId"]
    return data[0]["websiteId"]


def addLead(name,emailId,phoneNumber):
    url = 'http://34.230.154.142:7351/addLeadFromChatbot'
    # url = 'http://34.230.154.142:8482/addLeadFromChatbot'
    firstname = name.strip().split(' ')[0]
    lastname = ' '.join((name + ' ').split(' ')[1:]).strip()
    print firstname
    print lastname

    params = {
   'Title': "Mr./Ms.",
   'firstName': firstname,
   'middleName': "NA",
   'lastName': lastname,
   'workEmailId': emailId,
   'workphone': phoneNumber,
   'mobilephone':phoneNumber,
   'designation': "NA",
   'companyName': "NA",
   'industry': "NA",
   'companyWebsite': "NA",
   'numberOfEmployees': "NA",
   'annualRevenue': "NA",
   'sourceType': 1,
   'sourceId': 1,
   'leadType': "hot",
   'statusId': 1,
   'leadCategoryId' : 5,
   'leadCategoryName' : "",
   'leadDescription': "new opportunity",
   'leadTitle': "Wildlife"
  }
    headers = {'content-Type': 'application/json','Authorization':'a356789jk0987654321','device':'54','loggedInUserId':'4'}
    response = requests.post(url, data=json.dumps(params), headers=headers)
    print 'bheem'
    type(response)
    print response
    data = response.json()
    for reply in range(len(data)):
        data1 = data[reply]

    for key in data1:
        leadId = data1[key]

    return leadId