from __future__ import unicode_literals
from django.db import models

from PaxcelBotFramework.DecisionLogicLayer.models import SolverFactory

import datetime
from django.conf import settings
from DataAccessLayer.dbHandler import addLead
from DataAccessLayer.dbHandler import getWebsiteId
from PaxcelBotFramework.DataAccessLayer.chatlogsHandler import dbChatlogHandler
import json
from sendEmail import sendingUserDetails
from newUserIncommingMail import sendingNewUserDetails


# from MessageManager.messageHandler import createMessage
# from PaxcelBotFramework.BotManager.models.context import ContextHelperFunctions

import uuid

import logging
from json2html import *


logger = logging.getLogger(__name__)

# Define global flags for all the flows that you want to enable
flagAsianAdventure = False # global variable


# On() class
class On():
    def __init__(self, watsonOutput=None, operation=None):
        '''

        This is the input object to the Policy file of Intellect solver
        For more details read some examples in

        https://pypi.python.org/pypi/Intellect

        Solver Object Initialiser

        :param topIntent: the top most intent
        :param operation: function handle (modified by the policy file)
        '''

        self.watsonOutput = watsonOutput
        self.operation = operation

    @property
    def watsonOutput(self):
        return self.watsonOutput

    @watsonOutput.setter
    def watsonOutput(self, value):
        self.watsonOutput = value

    @property
    def operation(self):
        return self.operation

    @operation.setter
    def operation(self, value):
        self.operation = value
# End of On class


# Do() class
class Do():
    """
    This contains a list of all the actions based on the decision made by the rule engine
    each function will have a BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file) as input
    and will return a MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)
    the data access layer may be called as required, and the output is sent to the MessageFormationLayer for
    creation of message.
    """

    # Block 1: Handle Greetings
    @staticmethod
    def handleGreetings(botResponse):
        #accessing the global variables


        #initializations
        returnedComponentTray=[]
        returnedCompGreeting=[]
        returnedCompDurOrCat=[]
        context = botResponse["context"]

        domain = botResponse["botRequest"]["rawInput"]["data"]["domain"]
        url = botResponse["botRequest"]["rawInput"]["data"]["url"]
        botResponse["botRequest"]["info"]['url'] = url
        #id = getWebsiteId(domain)
        # ------
        botResponse["botRequest"]["info"]['websiteId'] = 1
        websiteId = botResponse["botRequest"]["info"]['websiteId']

        botResponse["botRequest"]["info"]['parentDataId'] = -1
        parentDataId = botResponse["botRequest"]["info"]['parentDataId']

        # ------



        conversationId = botResponse["botRequest"]["info"]['ConvSessionId']
        startTime = datetime.datetime.now()
        conversationId = botResponse["botRequest"]["info"]['ConvSessionId']
        startTime = datetime.datetime.now()
        chatlogsId = dbChatlogHandler.userConversationTimeDetails(
            conversationId, 0, startTime, startTime,
            settings.CONN_STRING)

        userMessage = 'url: ' + url
        botResponse["botRequest"]['info']['chatLogsConvId'] = chatlogsId
        botResponse["botRequest"]['info']['seqNo'] = 0
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)
        # Set the flags as per the domain


        # process the basic and info greetings
        returnedCompGreeting = processGreetings()

        # flag decides if asianAdventure flow will follow or some other category selection flow
        # ---------------------------
        returnedCompDurOrCat = processListFlow(websiteId,parentDataId)
        returnedComponentTray=returnedCompGreeting+returnedCompDurOrCat

        # ---------------------------


        # returnedComponentTray=returnedCompGreeting+returnedComponentTray
        return [botResponse, returnedComponentTray]

    @staticmethod
    def saveAccFormDetails(botResponse):
        # initializations
        returnedComponentTray = []

        context = botResponse["context"]

        checkInDate = botResponse["botRequest"]["rawInput"]["data"]["checkInDate"]
        checkOutDate = botResponse["botRequest"]["rawInput"]["data"]["checkOutDate"]
        # room = botResponse["botRequest"]["rawInput"]["data"]["room"]
        adults = botResponse["botRequest"]["rawInput"]["data"]["adults"]
        children = botResponse["botRequest"]["rawInput"]["data"]["children"]
        # userMessage = 'CheckInDate: ' + checkInDate + '; CheckOutDate: ' + checkOutDate + '; Room: ' + str(room) + '; Adults: ' + str(adults) + '; Children :' + str(children)
        userMessage = 'CheckInDate: ' + checkInDate + '; CheckOutDate: ' + checkOutDate + '; Adults: ' + str(adults) + '; Children :' + str(children)

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        comp1 = {
            "data": '',  # from data access layer
            "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        comp2 = {

            "componentType": "Form",
            "message": "",
            "name": str(uuid.uuid4()),  # a random name everytime
            "tag": "UserDetails",
            "nextCallTag": "Store_User_Details"

        }
        returnedComponentTray.append(comp1)
        returnedComponentTray.append(comp2)



        return [botResponse, returnedComponentTray]


    @staticmethod
    def saveUserDetails(botResponse):
        componentTray = []
        context = botResponse["context"]
        name = botResponse["botRequest"]["rawInput"]["data"]["name"]
        emailId = botResponse["botRequest"]["rawInput"]["data"]["emailId"]
        phoneNumber = botResponse["botRequest"]["rawInput"]["data"]["phoneNumber"]
        comments = botResponse["botRequest"]["rawInput"]["data"]["addComments"]

        url = botResponse["botRequest"]["info"]['url']

        userMessage = 'name: ' + name + '; emailId: ' + emailId + '; phoneNumber: ' + phoneNumber + '; comments: ' + str(
            comments)

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        leadId = addLead(name, emailId, phoneNumber)
        print type(leadId), 'bheem bheem'
        chatLogsConvId = botResponse["botRequest"]['info']['chatLogsConvId']
        dbChatlogHandler.updateUserIdBasedOnchatLogsConvId(leadId, chatLogsConvId, settings.CONN_STRING)
        logsArr = dbChatlogHandler.chatLogs(chatLogsConvId, leadId, settings.CONN_STRING)
        for var in range(len(logsArr)):
            tupleObj = logsArr[var]

        for t in range(len(tupleObj)):
            object2 = tupleObj[t]

        json_obj_in_html = ''

        for t in range(len(object2)):
            # object3 = object2[t]['message']
            if object2[t]['isbot'] == True:
                message = object2[t]['message']
                d = {}
                d['Joe'] = message

                json_obj_in_html += """\

                    <div style="margin-left: 30px"> 
                        <div style="display: flex; margin-bottom: 15px">
                        <div style="font-weight: bold; min-width: 80px; ">Joe</div><div style="">{message}</div>

                    </div></div>
                """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)
            else:
                message = object2[t]['message']
                d = {}
                d['User'] = message

                json_obj_in_html += """\
                    <div style="margin-left: 30px"> 
                        <div style="display: flex;margin-bottom: 15px">
                        <div style="font-weight: bold; min-width: 80px; ">User</div><div style="">{message}</div>

                    </div></div>
                                """.format(message=message)

                # json_obj_in_html += json2html.convert(json=d)

        sendingUserDetails(name, emailId, phoneNumber, url, comments, json_obj_in_html)

        compUserDetail1 = {
            "data": name,  # from data access layer
            "tag": "createMessageGreeting.namePresent",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(compUserDetail1)

        contactUs = {
            "data": "",  # from data access layer
            "tag": "createMessageContactUs.basic",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        componentTray.append(contactUs)

        mainMenu = {
            "data": "",  # from data access layer
            "tag": "mainMenu.data",  # this is for message formation layer
            "componentType": "Bubble",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }
        componentTray.append(mainMenu)
        return [botResponse, componentTray]

    # End of Do Class

    @staticmethod
    def handleBubbleData(botResponse):
        # accessing the global variables


        # initializations
        returnedComponentTray = []
        returnedCompGreeting = []
        returnedCompDurOrCat = []
        context = botResponse["context"]

        Id = botResponse["botRequest"]["rawInput"]["data"]["optionId"]
        Name = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        websiteId = botResponse["botRequest"]["info"]['websiteId']
        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']

        node_type_id = botResponse["botRequest"]["rawInput"]["data"]["node_type_id"]
        msgId = dbChatlogHandler.messagesChatlog(Name, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1

        print seqNo, 'seqno test'
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        if Name == "Domestic":
            botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
            compCatSelected = {
                "data": Name,  # from data access layer
                "tag": "createMessageGreeting.catSelected",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compCatSelected)


        if Name == "Honeymoon":
            botResponse["botRequest"]["info"]['rootName'] =botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        if Name == "Flight Deals":
            botResponse["botRequest"]["info"]['rootName'] =botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        if Name == "Hotel Packages":
            botResponse["botRequest"]["info"]['rootName'] =botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        if Name == "LTC Packages":
            botResponse["botRequest"]["info"]['rootName'] =botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        if Name == "Special Offers":
            botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]
        if Name == "Anything Else":
            botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]

        if Name == "International":
            botResponse["botRequest"]["info"]['rootName'] = botResponse["botRequest"]["rawInput"]["data"]["optionName"]

            compCatSelected = {
                "data": Name,  # from data access layer
                "tag": "createMessageGreeting.catSelected",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            returnedComponentTray.append(compCatSelected)
            # List 2 to be added or not?

        if node_type_id != 7 and Name not in ("others", "Anything Else"):
            compCategoryList2 = {
                "parentId": Id,
                "websiteId": websiteId,
                "data": "",  # from data access layer
                "tag": "createBotData.data",  # this is for message formation layer
                "componentType": "Bubble",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compCategoryList2)

        '''
        if node_type_id == 7 or Name in ("others", "Anything Else"):

            comp1 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            comp2 = {

                "componentType": "Form",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "UserDetails",
                "nextCallTag": "Store_User_Details"

            }
            returnedComponentTray.append(comp1)
            returnedComponentTray.append(comp2)
        

        '''
        if Name == "others" :

            comp1 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            comp2 = {

                "componentType": "Form",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "UserDetails",
                "nextCallTag": "Store_User_Details"

            }
            returnedComponentTray.append(comp1)
            returnedComponentTray.append(comp2)

        if Name in ("Anything Else"):
            askForOthers = {
                "data": '',  # from data access layer
                "tag": "createMessageAskForOthers.basic",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            textBoxForOthers = {
                "componentType": "TextBox",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "TextBox",
                "nextCallTag": "Handle_Text_Box"
            }

            returnedComponentTray.append(askForOthers)
            returnedComponentTray.append(textBoxForOthers)

            '''
            submitButton = {
                "data": "",  # from data access layer
                "tag": "submit.data",  # this is for message formation layer
                "componentType": "Bubble",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(submitButton)
            '''

        '''
        if Name == "Submit" and node_type_id == 16:
            comp1 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            comp2 = {

                "componentType": "Form",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "UserDetails",
                "nextCallTag": "Store_User_Details"

            }
            returnedComponentTray.append(comp1)
            returnedComponentTray.append(comp2)
         '''

        if node_type_id == 7 and Name != "others":
            comp1 = {
                "data": '',  # from data access layer
                "tag": "createMessageUserAccForm.basic",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }

            comp2 = {

                "componentType": "UserAccForm",
                "message": "",
                "name": str(uuid.uuid4()),  # a random name everytime
                "tag": "UserAccForm",
                "cancelNextCallTag": "Cancel_Acc_Details",
                "nextCallTag": "Store_User_BookYourAccommodation"

            }
            returnedComponentTray.append(comp1)
            returnedComponentTray.append(comp2)

        if node_type_id == 8 and Name not in ("Flight Deals", "Anything Else", "Special Offers", "Hotel Packages", "LTC Packages"):
            regionsSelected = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.selectRegions",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(regionsSelected)

        if node_type_id == 8 and Name in ("Hotel Packages","LTC Packages"):
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.durationSubCatPresent",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 9:
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.durationSubCatPresent",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 5 and botResponse["botRequest"]["info"]['rootName'] == "Domestic" and Name != "others":
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.bestPlace",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 5 and botResponse["botRequest"]["info"]['rootName'] == "International" and Name != "others":
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.bestCountry",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 5 and botResponse["botRequest"]["info"]['rootName'] == "Honeymoon" and Name != "others":
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.bestCountry",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 5 and botResponse["botRequest"]["info"]['rootName'] == "Hotel Packages" and Name != "others":
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.bestCountry",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if node_type_id == 5 and botResponse["botRequest"]["info"]['rootName'] == "LTC Packages" and Name != "others":
            compSubCat2 = {
                "data": '',  # from data access layer
                "tag": "createMessageGreeting.bestCountry",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compSubCat2)

        if Name == "Main Menu" :
            returnedComponentTray = []
            msgForMainMenu = {
                "data": '',  # from data access layer
                "tag": "createMessageForMainMenu.basic",  # this is for message formation layer
                "componentType": "SimpleMessage",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(msgForMainMenu)

            compCategoryList2 = {
                "parentId": -1,
                "websiteId": 1,
                "data": "",  # from data access layer
                "tag": "createBotData.data",  # this is for message formation layer
                "componentType": "Bubble",
                "message": "",  #
                "name": str(uuid.uuid4())  # a random name everytime
            }
            returnedComponentTray.append(compCategoryList2)

        return [botResponse, returnedComponentTray]

        # Block 7 Save the User Details and send email

    @staticmethod
    def handleTextBoxForOthers(botResponse):
        returnedComponentTray=[]
        context = botResponse["context"]
        # Id = botResponse["botRequest"]["rawInput"]["data"]["optionId"]
        userReq = botResponse["botRequest"]["rawInput"]["data"]["userReq"]
        Name = botResponse["botRequest"]["info"]['rootName']
        # websiteId = botResponse["botRequest"]["info"]['websiteId']
        userMessage = 'userReq: ' + userReq
        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        # node_type_id = botResponse["botRequest"]["rawInput"]["data"]["node_type_id"]
        msgId = dbChatlogHandler.messagesChatlog(userMessage, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1

        print seqNo, 'seqno test'
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        comp1 = {
            "data": '',  # from data access layer
            "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        comp2 = {

            "componentType": "Form",
            "message": "",
            "name": str(uuid.uuid4()),  # a random name everytime
            "tag": "UserDetails",
            "nextCallTag": "Store_User_Details"

        }
        returnedComponentTray.append(comp1)
        returnedComponentTray.append(comp2)
        return [botResponse, returnedComponentTray]

    @staticmethod
    def handleAccDetailsCancellation(botResponse):
        returnedComponentTray = []
        context = botResponse["context"]

        Name = botResponse["botRequest"]["info"]['rootName']

        chatlogsId = botResponse["botRequest"]['info']['chatLogsConvId']
        msgId = dbChatlogHandler.messagesChatlog(Name, settings.CONN_STRING)
        seqNo = botResponse["botRequest"]['info']['seqNo']
        seqNo = int(seqNo) + 1

        print seqNo, 'seqno test'
        botResponse["botRequest"]['info']['seqNo'] = seqNo
        dbChatlogHandler.messagesSequenceChatlogs(chatlogsId, seqNo, msgId, False,
                                                  settings.CONN_STRING)

        comp1 = {
            "data": '',  # from data access layer
            "tag": "createMessageGreeting.askForUserForm",  # this is for message formation layer
            "componentType": "SimpleMessage",
            "message": "",  #
            "name": str(uuid.uuid4())  # a random name everytime
        }

        comp2 = {

            "componentType": "Form",
            "message": "",
            "name": str(uuid.uuid4()),  # a random name everytime
            "tag": "UserDetails",
            "nextCallTag": "Store_User_Details"

        }
        returnedComponentTray.append(comp1)
        returnedComponentTray.append(comp2)
        return [botResponse, returnedComponentTray]


# processGreetings Function is responsible for fetching the greeting messages
def processGreetings():
    componentTray = []

    compGreetings1 = {
        "data": "",  # from data access layer
        "tag": "createMessageGreeting.basic",  # this is for message formation layer
        "componentType": "SimpleMessage",
        "message": "",  #
        "name": str(uuid.uuid4())  # a random name everytime
    }

    compGreetings2 = {
         "data": "",  # from data access layer
         "tag": "createMessageGreeting.about",  # this is for message formation layer
         "componentType": "SimpleMessage",
         "message": "",  #
         "name": str(uuid.uuid4())  # a random name everytime
     }
    compGreetings3 = {
         "data": '',  # from data access layer
         "tag": "createMessageGreeting.CatPresent",  # this is for message formation layer
         "componentType": "SimpleMessage",
         "message": "",  #
         "name": str(uuid.uuid4())  # a random name everytime
     }
    componentTray.append(compGreetings1)
    componentTray.append(compGreetings2)
    componentTray.append(compGreetings3)

    return componentTray


# processListFlow function is responsible for fetching the bubbles data
def processListFlow(websiteId, parentDataId):
    componentTray = []
    # compCategoryList1 = {
    #     "data": "",  # from data access layer
    #     "tag": "createMessageGreeting.service",  # this is for message formation layer
    #     "componentType": "SimpleMessage",
    #     "message": "",  #
    #     "name": str(uuid.uuid4())  # a random name everytime
    # }
    compCategoryList2 = {
        "parentId":parentDataId,
        "websiteId":websiteId,
        "data": "",  # from data access layer
        "tag": "createBotData.data",  # this is for message formation layer
        "componentType": "Bubble",
        "message": "",  #
        "name": str(uuid.uuid4())  # a random name everytime
    }
    componentTray.append(compCategoryList2)
    return componentTray


def processBotResponse(botManagerResponse):

    """

    Loads the Policy solver and performs policy solution expressions , returns messageLayerResponse object

    :param:  botResponse : A BotResponse() object (for definition see PaxcelBotFramework.Botmanager.models file)
    :return: messageLayerResponse : A MessageLayerModel() object (for definition see PaxcelBotFramework.MessageFormationLayer.models)


    """

    policyFilePath = settings.POLICY_FILE_PATH
    policySolver = SolverFactory().getSolverInstance(type=settings.SOLVER_TYPE, policyFilePath=policyFilePath)
    watsonOutput = botManagerResponse["outputMessages"][-1]["message"]

    print "watson output", watsonOutput
    logger.info(
        "User selection: {0}".format((watsonOutput))
    )
    OnObjectUpdated = policySolver.getResponse(On(watsonOutput=watsonOutput)) # on object the operation property has been updated
    [botResponse, componentTray] = OnObjectUpdated.operation(botManagerResponse)

    uiComponentTrayFromDecisionLayer = {
        "botResponse": botResponse,
        "uiComponents": componentTray

    }
    return uiComponentTrayFromDecisionLayer
